require(plyr)
require(reshape2)
require(ggplot2)

data <- read.csv('data_new.csv')
names(data)

max_values <- c(
    # Partner
                partner=2, # 1 - living with partner, 2 - no
    # Age
                agea=150,
    # Education
                edulvla=5,
    # SWB
                happy=10,   # 1 - extremely unhapyy
                stflife=10, # 1 - extremely unsatisfied
                gdsprt = 6, # 1 - all time good spirit
                clmrlx = 6, # 1 - all time calm and relaxed
    #Social activities
                sclmeet=10,
                sclact=5,
                #Felt lonely
                fltlnla=4, # 1 - No time, 4 - all time
  # Trust people
                ppltrst=10, # 1 - trust no one, 10 - trust most people
                pplfair=10, # same
  # Health
                health=5,
                hlthhmp=5,
                
  # Someone to discuss intimate things
                inmdisc=2,
  # Descriminated
                dscrgrp=5,
                #hhmb=25,
                wkdcorga=10,
                #hincsrca=8,
                hincfel=5,
                brwmny=5,
    #Values
                ipshabt=6,
                #ipubrst=6,
                #ipsucs=6,
                iprspot=6,
                ipcrtiv=6,
                imprich=6,
                ipeqopt=6,
                impdiff=6,
                ipfrule=6,
                ipudrst=6,
                ipmodst=6,
                ipgdtim=6,
                impfree=6,
                iphlppl=6,
                ipsuces=6,
                ipbhprp=6,
                iplylfr=6,
                impenv=6,
                imptrad=6,
                impfun=6,
                #
                stfjbot=10,
                rlgblg=10,
                
                domicil=5 #1 - big city, 5 - farm
  )

value_vars = c(ipshabt=6,
               iprspot=6,
               ipcrtiv=6,
               imprich=6,
               ipeqopt=6,
               impdiff=6,
               ipfrule=6,
               ipudrst=6,
               ipmodst=6,
               ipgdtim=6,
               impfree=6,
               iphlppl=6,
               ipsuces=6,
               ipbhprp=6,
               iplylfr=6,
               impenv=6,
               imptrad=6,
               impfun=6)
#important to be creative, to try new and different,
# to make own decisions and be free 
#ind_values = c("ipcrtiv", "impdiff", "impfree")
ind_values = c("ipcrtiv", "impdiff")
#imp to follow rules, to be modest, behave properly, follow traditions
trad_values = c("ipfrule", "ipmodst", "ipbhprp", "imptrad")
#imp to be rich, show abilities and be admired, 
#  to be successful and recognized, get respect
status_values = c("imprich", "ipshabt", "ipsuces", "iprspot")
#imp to be loyal to friends, help people, 
env_values = c("iplylfr", "iphlppl", "impenv", "ipeqopt")
#imp to seek fun, have a good time
fun_values = c("impfun", "ipgdtim")

all.values = c(ind_values,
               trad_values,
               status_values,
               env_values,
               fun_values)
#Set age_groups
data$age_group <- cut(data$agea, breaks=c(0, 17, 24, 35, 45, 65, 100),
                      labels=c("teens","young","yadult","adult","middle","old"))
head(data)
nrow(data)
#cdata$happy <- ifelse(cdata$happy > 10, NA, cdata$happy)

#with(data, {
#  e <- environment()
#  #ls(e)
#  sapply(names(max_values), function(x) {ifelse(get(x, envir=e)>max_values[x], NA, get(x, envir=e))})  
#})

# Set NA
for (i in names(data)) {
  if (i %in% names(max_values)) {
    data[,i] <- ifelse(data[,i] > max_values[i], NA, data[,i] )
  }  
}

# Calculate means for Values
v_data <- dcast(melt(data), cntry + age_group ~ variable, mean, na.rm=T,
                subset=.(variable %in% names(value_vars)))

#count_data <- dcast(melt(data), cntry + age_group ~ variable, length,
#                subset=.(variable %in% names(value_vars)))

#Plot it
#qplot(ipcrtiv, imprich, data = v_data) + geom_text(aes(ipcrtiv, imprich, label=cntry), data=v_data)

#
new_v_names <- as.vector(apply(as.array(names(v_data)), 1, FUN=paste, ".m", sep=""))
names(v_data) <- c("cntry", "age_group", new_v_names[3:20])
cdata <- join(data, v_data, by=c("cntry","age_group"))

m.ind_values <- paste(ind_values,".m", sep="")
m.trad_values <- paste(trad_values,".m", sep="")
m.status_values <- paste(status_values,".m", sep="")
m.env_values <- paste(env_values,".m", sep="")
m.fun_values <- paste(fun_values,".m", sep="")
m.values <- c(
  m.ind_values,
  m.trad_values,
  m.status_values,
  m.env_values,
  m.fun_values
  )

diff.ind_values <- paste(ind_values,".diff", sep="")
diff.trad_values <- paste(trad_values,".diff", sep="")
diff.status_values <- paste(status_values,".diff", sep="")
diff.env_values <- paste(env_values,".diff", sep="")
diff.fun_values <- paste(fun_values,".diff", sep="")
diff.values <- c(
  diff.ind_values,
  diff.trad_values,
  diff.status_values,
  diff.env_values,
  diff.fun_values
)

cdata[,diff.values] <- abs(cdata[,all.values] - cdata[,m.values])

protestant_cntr <- c("CH", "DK", "GB", "NL")
former_communist_cntr <- c("RU", "UA", "BG")

#hpp_cntr <- c("NO","CH","DK", "FI")

cdata$ind.fit <- rowMeans(abs(cdata[,diff.ind_values]))
cdata$trad.fit <- rowMeans(cdata[,diff.trad_values])
cdata$status.fit <- rowMeans(cdata[,diff.status_values])
cdata$fun.fit <- rowMeans(cdata[,diff.fun_values])

city.p <- cdata[cdata$domicil<=2,]
farm.p <- cdata[cdata$domicil>=3,]
young.p <- cdata[cdata$age_group %in% c("teens","young","yadult"),]
mature.p <- cdata[cdata$age_group %in% c("middle","old"),]

happy_means <- dcast(melt(cdata), cntry ~ age_group, mean, na.rm=T, subset=.(variable=="happy"))
happy_sd <- dcast(melt(cdata), cntry ~ age_group, sd, na.rm=T, subset=.(variable=="happy"))

require(lavaan)
require(semPlot)

hlmodel <- '
Cognitive =~ happy + stflife
Status =~ health + partner + edulvla
Cognitive ~~ Status
'
hlfit <- cfa(hlmodel, data = mature.p)
summary(hlfit, standardized=T, fit.measures=T)
modindices(hlfit)
semPaths(hlfit, "std","std")

pc.hlfit <- cfa(hlmodel, data = cdata) #[mature.p$cntry %in% protestant_cntr,])
summary(pc.hlfit, standardized=T, fit.measures=T)
#modindices(pc.hlfit)
semPaths(pc.hlfit, "std","std")

y.hlfit <- cfa(hlmodel, data = young.p)
summary(y.hlfit, standardized=T, fit.measures=T)
#modindices(y.hlfit)
semPaths(y.hlfit, "std","hide")

hlmodel <- '
Domain =~ stfjbot + hincfel
Free =~ health + partner + domicil
Domain ~ Status
'
hlfit <- cfa(hlmodel, data = mature.p)
summary(hlfit, standardized=T, fit.measures=T)



model <-'
#Latent
Cognitive =~ happy + stflife
#Affect =~ gdsprt +  clmrlx
#HLTH =~ health + hlthhmp
#STAT =~ partner + domicil
#Regressions
#FIT =~ ind.fit + status.fit
#Cognitive ~ STAT
status.fit + trad.fit ~ Cognitive
#STAT ~~ FIT
'
all_mod <- sem(model, cdata, meanstructure=T)
happ_mod <- sem(model, cdata[cdata$cntry %in% hpp_cntr,], meanstructure=T)
nh_mod <- sem(model, cdata[cdata$cntry %in% ee_cntr,], meanstructure=T)

summary(all_mod, standardized=T, fit.measures=T)
semPaths(all_mod, "std","hide")
summary(happ_mod, standardized=T, fit.measures=T)
semPaths(happ_mod, "std","hide")
summary(nh_mod, standardized=T, fit.measures=T)
semPaths(nh_mod, "std","hide")


e_wbmod <- cfa(model, cdata[cdata$cntry %in% ee_cntr,])
summary(e_wbmod, standardized=T, fit.measures=T)


semPaths(e_wbmod, "std","hide")
modindices(e_wbmod)

model.2 <- '
#Latent
Happiness =~ happy + stflife
Status =~ health + partner + edulvla
Fit =~ trad.fit

#Regressions
#Corellations
Happiness ~~ Status + Fit
'

fit2 <- cfa(model.2, cdata[cdata$cntry %in% former_communist_cntr,])
summary(fit2, standardized=T, fit.measures=T)



model.3 <- '
#Latent
Happiness =~ happy + stflife
IndValues =~ ipcrtiv + impdiff
#Sociable =~ sclmeet + sclact
#CulturalFit =~ rlgblg + dscrgrp

#Regressions
#CulturalFit ~ Sociable + Happiness

#Corellations
Happiness ~~ IndValues
'
fit3 <- cfa(model.3, cdata)
summary(fit3, standardized=T, fit.measures=T)

semPaths(fit3, "std","std")

model.3.a <- 'Cognitive =~ happy + stflife
              Status =~ health + partner + edulvla
              Fit =~ ind.fit + status.fit
              Cognitive ~ Status + Fit
'

fit3 <- cfa(model.3.a, cdata[cdata$cntry %in% protestant_cntr,])
summary(fit3, standardized=T, fit.measures=T)

semPaths(fit3, "std","std")


model.3.a <- 'Cognitive =~ happy + stflife
              Status =~ health + partner + edulvla
Fit =~ ind.fit + status.fit
Cognitive ~ Status + Fit
'

model.3.b <- 'Cognitive =~ happy + stflife
              Status =~ health + partner + edulvla
              Fit =~ trad.fit
              Cognitive ~~ Status + Fit'

model3.b.fit <- cfa(model.3.b, cdata[cdata$cntry %in% former_communist_cntr,])
summary(model3.b.fit, standardized=T, fit.measures=T)

semPaths(model3.b.fit, "std","std")


